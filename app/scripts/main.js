(function() {
  'use strict';
    function modal() {
      const modal     = document.getElementById('js-social-popup');
      const openBtn   = document.querySelector('.js-share-btn');
      const closeBtn  = document.querySelector('.js-close');

      function openModal(e) {
        e.preventDefault();
        modal.classList.add('is-open');
      }

      function closeModal() {
        modal.classList.remove('is-open');
      }

      openBtn.addEventListener('click', openModal);
      closeBtn.addEventListener('click', closeModal);
    }

    modal();

    var leftArrowElement = document.getElementsByClassName('carousel-left-arrow')[0];
    var rightArrowElement = document.getElementsByClassName('carousel-right-arrow')[0];

    var carousel = new Carousel({
        panels: document.getElementsByClassName('carousel-panel'),
        panelActiveClass: 'is-active',
        leftArrow: leftArrowElement,
        rightArrow: rightArrowElement,
        arrowDisabledClass: 'arrow-disabled'
    });

    carousel.goTo(0);
    rightArrowElement.click();
    leftArrowElement.click();
})();
